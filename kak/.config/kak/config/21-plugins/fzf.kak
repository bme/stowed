plug "andreyorst/fzf.kak" config %{
    map -docstring 'fzf mode' global normal '<c-p>' ': fzf-mode<ret>'
} defer "fzf" %{
    evaluate-commands %sh{
      if [ ! -z "$(command -v rg)" ]; then
        echo "set-option global fzf_file_command 'rg --files --follow --hidden -g \"!.git\"'"
      fi
    }
}
