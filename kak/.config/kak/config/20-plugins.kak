try %{
    evaluate-commands %{
        source "%val{config}/plugins/plug.kak/rc/plug.kak"
        plug "andreyorst/plug.kak" noload
    }
} catch %{
    echo -debug "Plugins: no plug.kak repository found"
}
