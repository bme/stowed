evaluate-commands %sh{
    load() {
        find -L "$1" -type f -name '*\.kak' -print | sort \
            | sed 's/.*/try %{ source "&" } catch %{ echo -debug Autoload: could not load "&" }/'
    }
    load "$kak_runtime/autoload"
    load "$kak_config/config"
}

