Ben's dotfile packages
======================

To use from this dir run `stow --target="$HOME" $pgk` where `$pkg` is the name of any folder in this directory. Of course you need to install GNU stow first, and any dependencies of the software listed. You can figure out.

HINT: use `-n -v` to see what stow is going to do before you let it rip.
